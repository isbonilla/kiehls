'use strict'

import store from '../src/store'
import i18n from '../src/js/plugins/i18n'

export default previewComponent => {
    return {
        store,
        i18n,
        render(createElement) {
            return createElement(previewComponent)
        }
    }
}
