'use strict'

export default {
    name: 'Dropdown',
    props: {
        /**
         * Close the previous open ones by default or keep them open if true.
         */
        showAll: {
            type: Boolean
        },

        /**
         * The main title section.
         */
        title: {
            type: [String, Object, Array]
        },

        /**
         * Array of Dropdown items content.
         */
        items: {
            type: Array
        }
    },
    data: () => ({
        lastRevealedIndex: -1
    }),
    methods: {
        handleAnimation(index) {
            const hasCollapsedItem = !!this.$el.querySelector('.collapse-show')

            if (!this.showAll && hasCollapsedItem && index !== this.lastRevealedIndex) {
                this.closeOpenedItems()
            }
            this.toggleItem(this.$el.querySelector(`.dropdown__item--${index}`))

            this.lastRevealedIndex = index
        },
        closeOpenedItems() {
            this.$el.querySelectorAll('.collapse-show').forEach(e => {
                e.classList.remove('collapse-show')
                e.querySelector(`.dropdown__item__description`).style.height = '0px'
            })
        },
        toggleItem(item) {
            const itemText = item.querySelector('.dropdown__item__description')
            const itemHeight = itemText.querySelector('p').clientHeight

            item.classList.toggle('collapse-show')
            itemText.style.height = (itemText.style.height === '' || itemText.style.height === '0px') ? `${itemHeight}px` : '0px'

            setTimeout(() => {
                this.$ScrollTrigger.refresh()
            }, (parseFloat(window.getComputedStyle(this.$el.querySelector('.dropdown__item__arrow')).transitionDuration)) * 1000)
        }
    }
}
