'use strict'

/**
 * Adds a column to a Frow container.<br />
 * Must be used inside a \<Frow>\</Frow> component.<br />
 * FrowCol is globally loaded, so you don't need to import it when you create a new block.
 */
export default {
    name: 'FrowCol',
    props: {
        /**
         * Size of the row for <b>mobile</b> breakpoints.<br />
         * The size is represented with a fraction of 12.<br />
         * Available values : `1-12` | `6-12` | `1-2` ...
         */
        mobCols: {
            type: String,
            default: '12'
        },

        /**
         * Size of the row for <b>tablet</b> breakpoints.<br />
         * The size is represented with a fraction of 12.<br />
         * Available values : `1-12` | `6-12` | `1-2` ...
         */
        tabCols: {
            type: String,
            default: '12'
        },

        /**
         * Size of the row for <b>desktop</b> breakpoints.<br />
         * The size is represented with a fraction of 12.<br />
         * Available values : `1-12` | `6-12` | `1-2` ...
         */
        deskCols: {
            type: String,
            default: '12'
        },

        /**
         * Set the column's background color.
         */
        backgroundColor: {
            type: String
        }
    }
}
