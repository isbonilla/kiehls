Examples :

```vue
<Frow>
    <FrowCol class="centered" mobCols="1-2" deskCols="1-6" backgroundColor="#ff9e80">
        <p>Column 1</p>
    </FrowCol>
    <FrowCol class="centered" mobCols="1-2" deskCols="1-6" backgroundColor="#ffe57f">
        <p>Column 2</p>
    </FrowCol>
    <FrowCol class="centered" mobCols="1-2" deskCols="1-6" backgroundColor="#ccff90">
        <p>Column 3</p>
    </FrowCol>
    <FrowCol class="centered" mobCols="1-2" deskCols="1-6" backgroundColor="#84ffff">
        <p>Column 4</p>
    </FrowCol>
    <FrowCol class="centered" mobCols="1-2" deskCols="1-6" backgroundColor="#ea80fc">
        <p>Column 5</p>
    </FrowCol>
    <FrowCol class="centered" mobCols="1-2" deskCols="1-6" backgroundColor="#ff8a80">
        <p>Column 6</p>
    </FrowCol>
</Frow>

<Frow>
    <FrowCol class="centered" deskCols="3-12" backgroundColor="#cfd8dc">
        <p>Column 1</p>
    </FrowCol>
    <FrowCol class="centered" deskCols="9-12" backgroundColor="#bdbdbd">
        <p>Column 2</p>
    </FrowCol>
</Frow>

<Frow class="justify-end">
    <FrowCol class="centered" deskCols="3-12" backgroundColor="#f3e5f5">
        <p>Column 1</p>
    </FrowCol>
    <FrowCol class="centered" deskCols="3-12" backgroundColor="#fbe9e7">
        <p>Column 2</p>
    </FrowCol>
</Frow>
```
