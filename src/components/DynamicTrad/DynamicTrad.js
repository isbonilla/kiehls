'use strict'

export default {
    name: 'DynamicTrad',
    props: {
        /**
         * The i18n key to display.<br />
         * Example : `legalMentions`
         */
        slug: {
            type: String,
            required: true
        },

        /**
         * The balise tag of the element.
         */
        tag: {
            type: String,
            default: 'p'
        }
    },
    methods: {
        getPathParameters(path) {
            let list = []
            let tmpList = path.split('{')

            for (let i = 0; i < tmpList.length; i++) {
                if (tmpList[i] !== '' && tmpList[i].indexOf('}') !== -1) list.push(tmpList[i].split('}').shift())
            }

            return list
        }
    }
}
