Handle result with slots. `prize` is the OP Code used for the Instant Win :

```vue
<SpinningWheel :segments="[
    { win: true, prize: 'spinningwheel_100' },
    { win: false },
    { win: true, prize: 'spinningwheel_200' },
    { win: true, prize: 'spinningwheel_100' },
    { win: false },
    { win: true, prize: 'spinningwheel_200' }
]">
    <template #win="{ selectedPrize }">
        <div>
            <p>You won</p>
            <p>Your prize : {{ selectedPrize }}</p>
        </div>
    </template>
    <template #lose>
        <div>
            <p>You lost</p>
        </div>
    </template>
</SpinningWheel>
```

Handle result with events :

```vue
<SpinningWheel
    :segments="[
        { win: true, prize: 'spinningwheel_100' },
        { win: false },
        { win: true, prize: 'spinningwheel_200' },
        { win: true, prize: 'spinningwheel_100' },
        { win: false },
        { win: true, prize: 'spinningwheel_200' }
    ]"
    @win="handleWin"
    @lose="handleLose"
/>

<!-- Then declare handleWin(prize) and handleLose(prize) functions in your methods -->
```

You can specify the collect you want to attach to the game using the `collectName` property. If not set, the value will be "default" :

```vue
<SpinningWheel
    collectName="another_collect"
    :segments="[
        { win: true, prize: 'spinningwheel_100' },
        { win: false },
        { win: true, prize: 'spinningwheel_200' },
        { win: true, prize: 'spinningwheel_100' },
        { win: false },
        { win: true, prize: 'spinningwheel_200' }
    ]"
/>
