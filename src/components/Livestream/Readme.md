All properties used except noButton :

```vue
<Livestream
    liveId="6347e3a0c895f8a0b2e256ad/6347e3e8c895f8a0b2e256b1"
    reminderAt="October 09, 2023 18:45:00"
    beforeLabel="Voir le live"
    startsAt="October 09, 2023 19:00:00"
    liveLabel="Voir le live"
    endsAt="October 09, 2023 19:30:00"
    afterLabel="Voir le replay"
/>
```

No default button (Call it in App.vue, line 3 before Loader call) :

```vue
<Livestream
    liveId="6347e3a0c895f8a0b2e256ad/6347e3e8c895f8a0b2e256b1"
    startsAt="October 09, 2023 19:00:00"
    noButton
/>
```

Use v-livestream directive on buttons (or anything else) that should make appear the Livestream.

Don't forget the type="live" property on your Click events so the label of the button updates 
regarding the live status and the beforeLabel, liveLabel and afterLabel props of the Livestream component.
Also used for the tracking ("Watch Livestream" event call, no "Click" event call as the usual other ones).

```vue
<Click id="btnId" v-livestream type="live" button />
```

Use label to specify different label from beforeLabel, liveLabel and afterLabel props of the Livestream component.

```vue
<Click id="btnId" v-livestream type="live" button :label="$store.getters['dumbo/getLink']('btnId')?.label" />
```
