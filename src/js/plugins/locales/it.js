export const it = {
    consent: {
        txt: [
            'Accedendo a questo contenuto',
            '<b> dichiari di avere almeno 18 anni.</b>'
        ],
        yes: 'Ho più di 18 anni',
        no: 'Ho meno di 18 anni'
    },
    header: {
        textscroll: 'Descubre la oferta'
    },
    offer: {
        discount1Title: '-25%',
        discount2Title: '-30%',
        discount1subtitle: 'EN TODO',
        discount2subtitle: 'en compras >150€<sup>(1)</sup>',
        discount3Title: 'DE REGALO',
        discount3subtitle: '3 minitallas + neceser',
        button: 'Aprovecha la oferta'
    }
}
