export const fr = {
    link: {
        backToHome: {
            trackingLabel: 'Back To Home',
            config: {
                type: 'home',
                options: {
                    home: {
                        slug: '',
                        id: ''
                    }
                }
            }
        },
        homeScroll: {
            trackingLabel: 'Home Scroll',
            config: {
                type: 'tracking',
                options: {}
            }
        },
        sticky: {
            label: 'J\'en profite',
            trackingLabel: 'Sticky',
            config: {
                type: 'direct',
                options: {
                    href: 'https://www.veepee.fr',
                    target: '_self'
                }
            }
        }
    },
    sticky: {
        text: { type: 'smallBody', text: 'Short reminder of <strong>the offer, xx% off</strong> + the <strong>promo code</strong>' }
    },
    timer: {
        title: "Veepee's Exclu of the day",
        timerLabels: {
            day: 'JOUR',
            hour: 'HEURES',
            minute: 'MINUTES',
            second: 'SECONDES'
        }
    },
    consent: {
        txt: [
            'En accédant à ce contenu, ',
            '<b>vous déclarez avoir 18 ans ou plus.</b>'
        ],
        yes: 'J’ai plus de 18 ans',
        no: 'J’ai moins de 18 ans'
    },
    carteVerte: [
        'Pour une consommation plus responsable.',
        'Découvrez les engagements des marques pour un monde plus durable, plus solidaire ou plus respectueux des limites de la planète.',
        'Bénéficie​z​ d’un avantage exclusif, profite​z d’​un bon plan ou découvr​ez une nouvelle collection.'
    ],
    tel: {
        placeholder: 'téléphone'
    },
    form: {
        fields: {
            lastname: 'Nom',
            firstname: 'Prénom',
            email: 'E-mail',
            rules: 'J’accepte le règlement',
            optin: 'J’accepte de recevoir des offres'
        },
        send: 'Envoyer'
    },
    cp: {
        placeholderZip: 'Postal code',
        invalidZip: 'Code postal invalide'
    },
    live: {
        before: {
            tracking: 'Before',
            label: 'Voir le live'
        },
        during: {
            tracking: 'On Time',
            label: 'Voir le live'
        },
        after: {
            tracking: 'After',
            label: 'Voir le replay'
        },
        trackingDependencies: { '35078032': 'OUI' } // set IDs of products there and their tracking label. Example : '300': 'SPARTE'
    },
    mailApi: {
        'firstname is required': 'Le champs "Prénom" est requis',
        'lastname is required': 'Le champs "Nom" est requis',
        'rules is required': 'Vous devez accepter le règlement du jeu',
        'Missing email destination': 'Adresse e-mail manquante',
        'User already exist': 'Adresse e-mail déjà utilisée',
        'Recipient address must be unique': 'Adresse e-mail déjà utilisée',
        'Invalid email': 'Adresse e-mail invalide',
        'Invalid pc': 'Code postal invalide',
        'One or more fields are invalid': 'Un ou plusieurs champs sont invalides'
    },
    AlertWebapp: '"Pour accéder à la vente, téléchargez l\'application Veepee ou connectez-vous depuis votre ordinateur."',
    copycode: {
        text: 'You can copy this code so you have an access to Sparta',
        code: 'AHOO',
        buttonLabel: 'THIS IS SPARTA !',
        copyLabel: 'Cliquez pour copier',
        copiedLabel: 'Le code a été copié'
    },
    footerMentions: 'Marque'
}
