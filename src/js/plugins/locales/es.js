export const es = {
    consent: {
        txt: [
            'Accediendo a este contenido ',
            '<b>declaras tener 18 años o más.</b>'
        ],
        yes: 'Tengo más de 18 años',
        no: 'Tengo menos de 18 años'
    },
    header: {
        textscroll: 'Descubre la oferta'
    },
    offer: {
        discount1Title: '-25%',
        discount2Title: '-30%',
        discount1subtitle: 'EN TODO',
        discount2subtitle: 'en compras >150€<sup>(1)</sup>',
        discount3Title: 'DE REGALO',
        discount3subtitle: '3 minitallas + neceser',
        button: 'Aprovecha la oferta'
    },
    slider: {
        slider1Title: 'Super Multi-Corrective Cream SPF 30',
        slider1Subtitle: 'Crema antiarrugas contra los signos de la edad.',
        slider1Content: '<b>96%</b>textura refinada<sup>(4)</sup><br><b>94%</b>piel más firme<sup>(4)</sup><br><b>92%</b> arrugas reducidas<sup>(4)</sup>',
        slider2Title: 'Ultra Facial Cream',
        slider2Subtitle: 'Crema hidratante textura ligera.',
        slider2Content: '<b>18%</b>piel más suave<sup>(3)</sup><br><b>33%</b>piel más ligera<sup>(3)</sup><br><b>11%</b> piel más saludable<sup>(3)</sup>',
        slider3Title: 'Retinol Skin-Renewing Daily Micro-Dose',
        slider3Subtitle: 'Potente sérum con Retinol anti-edad.',
        slider3Content: '<b>100%</b>arrugas reducidas<sup>(5)</sup><br><b>90%</b>piel más firme<sup>(5)</sup><br><b>94%</b>textura refinada<sup>(5)</sup>'
    },
    categories: {
        title: 'Cuida tu piel con <b>-25% de descuento en todo <br class="no-desktop"> y -30% por compras superiores a 150€<sup>(1)</sup></b>.',
        card1: 'BEST SELLERS',
        card2: 'ROSTRO',
        card3: 'CUERPO',
        card4: 'CABELLO'
    },
    sticky: {
        text: '',
        label: 'Aprovecha la oferta'
    },
    mention: {
        txt: '<sup>(1)</sup>Promoción válida del 28 al 29 de Febrero 2024. Promoción no acumulable con otras promociones activas. Minitallas y neceser sujetos a disponibilidad.<br><sup>(2)</sup>Por compras superiores a 130€ ya descontados (169€ P.V.P sin descontar) y se añade a la cesta automáticamente sin códigos.<br><sup>(3)</sup>Resultados basados en un ensayo clínico de cuatro semanas en 55 individuos.<br><sup>(4)</sup>Resultados basados en un ensayo clínico de doce semanas en 53 individuos.<br><sup>(5)</sup>Resultados basados en un ensayo clínico de doce semanas.'
    },
    footerMentions: 'Todos los derechos reservados.'
}
