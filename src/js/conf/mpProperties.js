'use strict'

import { getQueryString } from '@/js/tools'
import lang from '@/js/conf/lang'
import common from '@/js/conf/common'

let queryString = getQueryString('mp_properties', null)
let mpProperties = []

if (queryString) {
    // Get mp_properties from query string (Destin and iOS/Android app)
    queryString = decodeURIComponent(queryString)
    queryString = atob(queryString)
    mpProperties = JSON.parse(queryString)
    console.log('querystring')
} else {
    // Get mp_properties from localstorage (desktop or webview with Groot)
    const mixpanelToken = process.env.NODE_ENV === 'production' ? common.mixpanel.prod.token[lang.site][lang.domain] : common.mixpanel.preprod.token
    const localStorage = window.localStorage
    if (localStorage[`mp_${mixpanelToken}_mixpanel`]) {
        mpProperties = JSON.parse(localStorage[`mp_${mixpanelToken}_mixpanel`])
    }
}

export default mpProperties
