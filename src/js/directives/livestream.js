'use strict'

const livestream = {
    bind: (element, binding, vnode) => {
        element.addEventListener('click', () => {
            vnode.context.$root.$emit('openLivestreamModal')
        })
    }
}

export {
    livestream
}
