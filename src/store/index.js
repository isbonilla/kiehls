import Vue from 'vue'
import Vuex from 'vuex'
import loader from './modules/loader'
import member from './modules/member'
import debug from './modules/debug'
import general from './modules/general'
import livestream from './modules/livestream'
import easterEgg from './modules/easterEgg'
import mixpanel from './modules/mixpanel'
import dumbo from './modules/dumbo'

Vue.use(Vuex)

const store = new Vuex.Store({
    modules: {
        loader,
        member,
        debug,
        general,
        livestream,
        easterEgg,
        mixpanel,
        dumbo
    }
})

export default store
