'use strict'

const state = () => ({
    config: {},
    links: []
})

const getters = {
    getLink: state => id => {
        return state.links[id]
    }
}

const actions = {
    setConfig: (context, config) => context.commit('setConfig', config),
    setLinks: (context, links) => context.commit('setLinks', links)
}

const mutations = {
    setLinks: (state, links) => { state.links = links },
    setConfig: (state, config) => { state.config = config }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
