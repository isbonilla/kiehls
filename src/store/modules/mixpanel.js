'use strict'

import common from '@/js/conf/common'
import mpProperties from '@/js/conf/mpProperties'
import lang from '@/js/conf/lang'

let state = () => ({
    mixpanel: require('mixpanel-browser'),
    memberID: -1,
    mixpanelID: -1
})

const getters = {
    getMixpanel: state => state.mixpanel
}

const actions = {
    initMixpanel: (context, member) => context.commit('initMixpanel', member)
}

const mutations = {
    initMixpanel: (state, member) => {
        // Load mixpanel configuration
        let mixpanelApiHost, mixpanelToken
        if (process.env.NODE_ENV === 'production') {
            mixpanelApiHost = common.mixpanel.prod.apiHost
            mixpanelToken = common.mixpanel.prod.token[lang.site][lang.domain]
        } else {
            mixpanelApiHost = common.mixpanel.preprod.apiHost
            mixpanelToken = common.mixpanel.preprod.token
        }

        // Initialize Mixpanel
        state.mixpanel.init(mixpanelToken, {
            api_host: mixpanelApiHost,
            disable_notifications: true,
            autotrack: false,
            track_pageview: false,
            persistence: 'localStorage',
            debug: process.env.NODE_ENV !== 'production'
        })

        // Identify mixpanel member
        state.mixpanel.identify(member.id)

        // Register super properties
        state.mixpanel.register({
            'Interface': mpProperties['Interface'],
            'UTM Term [Last Touch]': mpProperties['UTM Term [Last Touch]'],
            'UTM Content [Last Touch]': mpProperties['UTM Content [Last Touch]'],
            'UTM Campaign [Last Touch]': mpProperties['UTM Campaign [Last Touch]'],
            'UTM Medium [Last Touch]': mpProperties['UTM Medium [Last Touch]'],
            'UTM Source [Last Touch]': mpProperties['UTM Source [Last Touch]'],
            'CRM Segment': mpProperties['CRM Segment'],
            '# of Completed Purchases': mpProperties['# of Completed Purchases'],
            'Member ID': member.id,
            'Optimizely Current A/B Tests': mpProperties['Optimizely Current A/B Tests'],
            'Source': mpProperties['Source'],
            'Point Reached On Homepage': mpProperties['Point Reached On Homepage']
        })

        state.memberID = member.id
        state.mixpanelID = state.mixpanel.get_distinct_id()
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
