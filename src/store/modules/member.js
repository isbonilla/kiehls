'use strict'

import getMemberID from '@/js/conf/memberID'
import config from '@/js/conf/common'
import axios from 'axios'

const state = () => ({
    id: null,
    hash: null
})

const mutations = {
    setMemberId: (state, memberId) => {
        state.id = memberId
    },
    hashMemberId: (state, hash) => {
        state.hash = hash
    }
}

const actions = {
    setMemberId: context => {
        getMemberID.then(member => {
            context.commit('setMemberId', member.id)
        })
    },
    hashMemberId: context => {
        if (config.modules.hashMemberId) {
            getMemberID.then(member => {
                let apiUrl = process.env.NODE_ENV === 'production' ? config.api.prod.hashMemberId : config.api.preprod.hashMemberId

                axios.post(apiUrl, `member_id=${member.id}`)
                    .then(response => {
                        context.commit('hashMemberId', response.data.partner_id)
                    })
                    .catch(error => console.log(error))
            })
        }
    }
}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}
