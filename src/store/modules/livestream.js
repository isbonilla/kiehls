'use strict'

let state = {
    liveOn: false,
    startsAt: 'October 09, 2023 19:00:00',
    endsAt: null,
    reminderAt: null,
    liveStatus: 'before',
    liveStatusTracking: 'Before',
    beforeLabel: '',
    liveLabel: '',
    afterLabel: '',
    btnLabel: '',
    shopOpen: false
}

const getters = {
    getLiveReminder: state => {
        return state.reminderAt ? new Date(new Date(state.startsAt) - new Date(state.reminderAt)) : new Date(state.startsAt)
    },
    getLiveStartMax: state => {
        const minutes = 10 // default minutes we consider the live ended if the livestream liveOn prop is off
        return state.endsAt ? new Date(state.endsAt) : new Date(new Date(state.startsAt).getTime() + (1000 * 60 * minutes))
    }
}

const actions = {
    setLiveData: (context, live) => context.commit('setLiveData', live),
    setStatus: (context, isStreamOn) => context.commit('setStatus', isStreamOn),
    toggleShop: (context, showShop) => context.commit('toggleShop', showShop)
}

const mutations = {
    setLiveData: (state, live) => {
        // Btn labels
        state.beforeLabel = live.beforeLabel
        state.liveLabel = live.liveLabel
        state.afterLabel = live.afterLabel

        // Status dates
        state.startsAt = live.startsAt
        if (live.reminderAt) state.reminderAt = live.reminderAt
        if (live.endsAt) state.endsAt = live.endsAt
    },
    setStatus: (state, isStreamOn) => {
        if (isStreamOn) {
            state.btnLabel = state.liveLabel
            state.liveStatus = 'live'
            state.liveStatusTracking = 'On Time'
            state.liveOn = true
        } else {
            state.liveOn = false

            if (new Date() <= getters.getLiveStartMax(state)) {
                state.btnLabel = state.beforeLabel
                state.liveStatus = 'before'
                state.liveStatusTracking = 'Before'
            } else {
                state.btnLabel = state.afterLabel
                state.liveStatus = 'after'
                state.liveStatusTracking = 'After'
            }
        }
    },
    toggleShop: (state, showShop) => {
        state.shopOpen = showShop
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
