# Hashing member ID

> You will need to set the hashMemberId variable to true in the `src/js/conf/common.js` file

```js
module.exports = {
    modules: {
        hashMemberId: true
    }
}
```

> Insert the hashed member ID inside the href of any link

```html
    <a :href="`https://https://www.hellobank.fr/?code_partenaire=${this.$store.state.member.hash}`">
        J'en profite
    </a>
```

Member's ID and hashed ID are stored inside a new member vuex module so they can be used anywhere on the website like this:

Get member id:

`this.$store.state.member.id`

Get hashed member id:

`this.$store.state.member.hash`

Hashed member ID can only be accessed if the `hashMemberId` variable is set to true inside the `src/js/conf/common.js` file (`false` by default)

<aside class="warning">The clear member ID should NEVER be sent to the client</aside>
