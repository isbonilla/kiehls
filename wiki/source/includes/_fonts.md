# Fonts

> Create a folder for your webfonts, and a .scss file

```text
src
├── assets
│   └── fonts
│       └── vp-sans-next
│           ├── VPSansNext-Regular.eot
│           └── VPSansNext-Bold.eot
└── styles
    └── vars
        ├── fonts
        │   └── _vpsansnext.scss
        └── _fonts.scss
```

> Declare the webfont with the `font-face()` mixin in `_vpsansnext.scss`

```css
/* font-face($name, $file, $folder, $category, $weight, $style) */
@include font-face('vp-sans-next', 'VPSansNext-Regular', 'vp-sans-next', 'serif', normal, normal);
@include font-face('vp-sans-next', 'VPSansNext-Bold', 'vp-sans-next', 'serif', bold, normal);
```

> Import the webfont declaration in `_fonts.scss`

```css
@import "./fonts/vpsansnext";

/* Use a variable to easily call your webfont later on */
$t-vpsansnext: "vp-sans-next";
```


1. **Convert your font to a webfont**

In order to add a font to your website, you first need to convert it to a webfonts using one of the following tools :
- https://www.fontsquirrel.com/tools/webfont-generator
- https://transfonter.org/

2. **Add the webfont to the fonts folder**

Copy your fonts and its variants (light, medium, bold, italic, ...) in a new folder here `@/assets/fonts/your-new-font/`

3. **Declare your font**

Create a .scss file here `@/assets/styles/vars/fonts/_your-new-font.scss` use the `font-face()` mixin to register as many variants as needed.

Finally, import this new file in the main file: `@/assets/styles/vars/_fonts.scss`
