# Tips & Tricks

## Display

```html
<p class="no-desktop">Only visible on mobile devices</p>
<p class="desktop-only">Only visible on desktop devices</p>

<p v-if="!this.$config.detectEnv.isDesktop">Only visible on mobile devices</p>
<p v-if="this.$config.detectEnv.isDesktop">Only visible on desktop devices</p>
```

You can use 3 methods to display an element on specified devices :
- The css classes (`mob-only`, `desktop-only`, `no-mob`, `no-desktop`, `app-only`, `no-app`)
- The directives (cf the Directives section)
- The detectEnv in a VueJs condition (`this.$config.detectEnv.isDesktop` and `!this.$config.detectEnv.isDesktop`)

## Disable scrolling

> Inside a vue template file

```html
<Click button label="disable scroll" @click.native="$disableScroll()" />
<Click button label="enable scroll" @click.native="$enableScroll()" />
```

> Inside a javascript file

```javascript
// Disable scroll
this.$disableScroll()

// Enable scroll
this.$enableScroll()
```

Disable scrolling using the `$disableScroll()` function.

Scrolling can then be reactivated with `$enableScroll()`.

## Quickly create a new Block component

> Call the script in the terminal, from your project's root folder

```sh
./scrips/create_new_block.sh YourComponentName
```

> New files will be created

```text
src
└── block
    └── YourComponentName
        ├── NewBlock.js
        ├── NewBlock.scss
        └── NewBlock.vue
```

Use this helper script to quickly create and add a new Block components to your Vue application:

`./scrips/create_new_block.sh YourComponentName`

The script will :

- Create a new folder in `src/blocks/YourComponentName` with the 3 files `YourComponentName.vue`, `YourComponentName.scss` and `YourComponentName.js`
- Import the `YourComponentName` component in `src/App.js`
- Call the `YourComponentName` component in `src/App.vue`

## Overriding CSS with ::v-deep

> HTML file

```html
<Parent>
    <Child class="child">
        <p>I want to be red</p>
    </Child>
</Parent>
```

> Child's CSS file (scoped)

```css
.child p {
    color: blue;
}
```

> Parent's CSS file (scoped)

```css
/* You need to add ::v-deep to override the scoped CSS of the child component */
::v-deep .child p {
    color: red;
}
```

`::v-deep` is a usefull pseudo-selector to override the scoped CSS of any children element from the parent.

## .native event modifier

> Vue template

```html
<Child @click.native="functionToLaunch()" />
```

There may be times when you want to listen directly to a native event on the root element of a component.

In these cases, you can use the .native modifier for your event (@click, @blur, ...)

<aside class="notice">More info in the <a href="https://vuejs.org/v2/guide/components-custom-events.html#Binding-Native-Events-to-Components">vuejs.org</a> documentation</aside>

## Debug panel

Add `#debug` at the end of your URL to display the debug panel:

http://localhost:8080/#debug

This panel will let you check your tracking information quickly.

You can also open your website in the Veepee app for testing purposes.

<aside class="notice">The debug panel only works on development or preproduction environment</aside>

## The "@" path shortcut

The "@" symbol is an alias (or shortcut) to the /src folder.

You can use it in a .vue file or a .js file to call an asset or import a file.

> Import a script or a component

```js
import Img from '@/components/Img/Img.vue'
```

> Load an image

```html
<img src="@/assets/img/logo.png" />
```

## Test your website on device

> Run your server

```sh
npm run serve
```

> You can access your website by using the "Network" IP address on your mobile device's browser

```text
App running at:
  - Local:   http://localhost:8080
  - Network: http://192.168.1.16:8080
```

> If you run a VPN, get your ip address with this command from the terminal

```
ifconfig
```

> Then search for your ip

```html
example : 192.168.1.16
```

To test your website on device, your mobile and destkop devices need to be on the same network.
You can see the url in your terminal when the website is ready (for example : 192.168.1.16), so you just have to open your mobile navigator and paste the url given.

If you're connected with a VPN, you can deactivate it, because you'll see that you only have a localhost url, that can't be used on your mobile device, and the ip of your VPN (and you can't connect your mobile device to the same VPN)

**But**, you can also check your ip without disconnect from the VPN. You just have to open a terminal, and then check your ip with the `ifconfig` command line.
It will show you many information, but you just have to search the en0 ip

As shown below, you see the `ifconfig` command line (framed in pink/purple), the **en0** (framed in yellow) and the **ip** (framed in red).

![ifconfig example](./images/ifconfig.png)
