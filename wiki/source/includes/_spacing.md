# Spacing

> Some examples…

```html
<!-- 16px padding in all directions -->
<div class="pa-2">
    <!-- 8px margin bottom -->
    <h1 class="mb-1">Title</h1>
    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>

    <!-- 8px margin top + bottom -->
    <hr class="my-1" />

    <!-- centered image with auto margin left + right -->
    <img src="https://picsum.photos/seed/picsum/200/300" style="display: block; width: 100px" class="mx-auto"> 
</div>
```
> Script to run to compile by changing '_[unit]Spacing.scss' to the value used in the '_du' file. For example for rem will be _remSpacing.scss

```js
    node ./node_modules/sass/sass.js --no-source-map ./src/assets/styles/compiled/_spacing.scss ./src/assets/styles/compiled/_[unit]Spacing.scss
```

> Add mixin in compiled file : _[unit]Spacing.scss

```scss
    @mixin [unit]Spacing { 
        [.. compiled file ..]
    }
```

Use the helper classes inspired by Vuetify to add margin and padding to any element without CSS.

The default spacing value is 8px and can be changed in the `/src/assets/styles/partials/_spacing.scss`

<aside class="notice">Only compiled versions are used : <br/>
`/src/assets/styles/compiled/_remSpacing.scss` for the unity 'rem'<br/>
`/src/assets/styles/compiled/_vhSpacing.scss` for the unity 'vh'<br/>
`/src/assets/styles/compiled/_vwSpacing.scss` for the unity 'vw'<br/>
In case of modification it will be necessary to recompile the base file `/src/assets/styles/compiled/_spacing.scss` by selecting the value defined in '/src/assets/styles/vars/_du' : <br/>
Script to run to compile by changing '_[unit]Spacing.scss' to the value used in the '_du' file. For example for rem will be _remSpacing.scss `node ./node_modules/sass/sass.js --no-source-map ./src/assets/styles/compiled/_spacing.scss ./src/assets/styles/compiled/_[unit]Spacing.scss`<br/>
After compilation it will be necessary to add a mixin of the same name in the compiled file by emglobant the content.
</aside>

Documentation : https://vuetifyjs.com/en/styles/spacing/#how-it-works