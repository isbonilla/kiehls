# Private npm registry

Guide to enable the private package registry (.npmrc file)

1. In gitlab, go to « Access Token » : https://git.vptech.eu/-/profile/personal_access_tokens
2. Click « Add a new token »,
3. Name the token « brandmanagement-npmrc »,
4. Remove the expiration date,
5. Enable « api » scope,
6. Submit,
7. Enter the following command in the terminal. Replace YOUR_TOKEN_HERE with the token created right above,

> (step 7) : Add the .npmrc file with your token into your root folder

```sh
echo '@brandmanagement:registry=https://git.vptech.eu/api/v4/projects/13447/packages/npm/
//git.vptech.eu/api/v4/projects/13447/packages/npm/:_authToken="YOUR_TOKEN_HERE"' > ~/.npmrc
```

8. You can now install packages from the @brandmanagement registry

> (step 8) : Install package from the @brandmanagement registry

```shell
npm install @brandmanagement/links
```
