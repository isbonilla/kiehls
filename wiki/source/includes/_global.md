# Global variable

> Create a global function

```js
// Create and store the sayHello() function in a constant
const sayHello = (name) => {
    console.log(`Hello ${name}!`)
};

// Add it to the Vue.prototype
Vue.prototype.$sayHello = sayHello;
```

> Use it in your component.vue file

```html
<!-- Inside a .vue template file -->
<p>{{ $sayHello('world') }}</p>
```

> Use it in your component.js file

```js
// Inside a .js component file
this.$sayHello('Media design team')
```

You can create any global variable (including functions) by adding it to the `Vue.prototype` object:

`Vue.prototype.$hello = "Hello world!";`

The variable can then be called using the following syntax:
- `{{ $hello }}` : from a component.vue template file
- `this.$hello` : from a component.js file

<aside class="notice">A `$config` variable exists in the project.
It contains several useful information for your project.
Check the <a href="https://git.vptech.eu/veepee/offerdiscovery/products/brandmanagement/base-vuejs/-/blob/master/src/js/conf/index.js" target="_blank">`@/src/js/conf/index.js`</a> file for more information .</aside>
