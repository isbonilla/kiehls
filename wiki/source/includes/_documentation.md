# Documentation

## Components Documentation

### Codepen

https://codepen.io/studio_vpad_dev/
There you can find inspiration and code that the team already did.

### DUMBO

https://brand-dumbo.immaterial.vptech.eu/
DUMBO is our tool to manage the operations. You can check the planning, create an operation or manage it by setting the links, the medias, the trads and other things.
Don't forget to Save then Publish !

### BaseVue

http://preprod.veepee.fr/experiences/base-vuejs/doc/
On that document, you'll find all components we use, and how to use them.


## Global Documentation

### Wiki

You are on the Wiki. You'll find many technical information and tips about the tools we use.

### Welcome Doc

https://docs.google.com/document/d/1NGPj4qAb9O2GCfPDDHzBk0ADO9onHLUhv-aeEHZlsyk/edit#
This is a document that will explain you **all the basic information**. More likely useful for new devs.

### Confluence

https://confluence.vptech.eu/display/TT/Immaterial+Media
You can find many **global information on our team** there.

### Minisites Optimisation

https://docs.google.com/document/d/1ZC6en1vGClbYF8l7zb7BT7RJHnoIS5I3DmXcGNJRBIU/edit
You'll find some technical guidelines for the mini sites optimisation.

### Landing Pages / Livestream

https://docs.google.com/document/d/1HUoAjBV7rTPDhbhnukcknh3GnySFZ1SD0ZpjUCfMYFo/edit#heading=h.owij7pwsepo3
Here's a tutorial on how Landing Pages and Livestreams works with Redis
