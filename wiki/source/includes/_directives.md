# Directives

## v-scroll-to

> Default example in Heaer.vue

```html
<div v-scroll-to="'.app__container'"></div>
```

You can find an example of a v-scroll-to use on the Home, in BaseVue.

You just have to set the v-scroll-to prop to use it, and to specify which element you'll reach.

If this is a class or an id, do not forget the "." or the "#" before the name, like in the example beside.

## Parallax

```html
   <div v-parallax="3" />
```

The `parallax` directive has a value of 10 by default.
You can pass the speed in the param as shown in example. The higher the value, the lower the speed.

## Display

```html
<p v-display:mob>Only visible on mobile devices</p>
<p v-display:desktop>Only visible on desktop devices</p>
```

You can use one of these to display your element (`mob`, `mob-only`, `desktop`, `desktop-only`, `no-mob`, `no-desktop`, `app`, `no-app`)

<aside class="notice">Those like `mob` and `mob-only` are the same. You can choose your favorite.</aside>

## Timestamp

```html
<p v-html="'before'" v-timestamp:before="'December 22, 2021 14:38:00'" />
<p v-html="'before default date'" v-timestamp:before />
<p v-html="'between'" v-timestamp:between="['December 22, 2021 14:38:00', 'December 22, 2021 14:38:10']" />
<p v-html="'after'" v-timestamp:after="'December 22, 2021 14:38:00'" />
```

The `timestamp` directive allows to show or hide elements.

To use it, you need to specify when you want to see the element, and the date (optional). For example, if you want to see an element before a given date, you'll specify `before` and the date, as you can see in given examples aside.
If you have just one date, you can update the default one in `common.js` (the `operation.date`).

<aside class="notice">Please note that elements with this directive will be displayed as 'block', or 'none' if hidden.</aside>

```
http://localhost:8080/?previewDate=December 01, 2024 14:38:00
http://localhost:8080/?previewDate=2024/12/01
```

<aside class="notice">If you want to check your website from another date, use the previewDate parameter as shown alongside</aside>

## Livestream

```vue
<Click id="btnId" v-livestream button />
<Click id="btnId" v-livestream block>
  <Img src="https://dummyimage.com/720x574/bababa/aaa" />
</Click>
```

The `livestream` directive allows any element to be a clickable element that will make the livestream appear on click.

<aside class="notice">Please do not forget to import the Livestream component in App.vue to make it work.</aside>

<aside class="notice">Please note that there isn't any style with the directive, such as cursor pointer. So don't forget to apply css.</aside>
