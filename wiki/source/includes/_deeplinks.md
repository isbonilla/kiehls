# Deeplinks

Deeplinks are links that interracts with the mobile Veepee app installed on your smartphone.

<aside class="notice">Most usefull links can be accessed via helper functions. See the <a href="#Links">Links section</a>.</aside>

> Examples

```html
<!-- Link to the "Travel" homepage -->
<a href="appvp://home/64 ">Veepee Travel</a>

<!-- Link to an operation -->
<a href="appvp://operation/98924">Access to the sale</a>
```

| Deeplink*                                               | Action                                                                                                           |
| --------------------------------------------------------|------------------------------------------------------------------------------------------------------------------|
| appvp://home                                            | Open the operations home page                                                                                    |
| appvp://home/{homeId}                                   | Open the operations home page at the specific home *#homeId*                                                     |
| appvp://subscribebrand/{codeOpe}                        | Subscribe to a brand with the operation code *codeOpe*                                                           |
| appvp://aggregation/{id}                                | Open operations aggregation page by using the *#campaignId*                                                      |
| appvp://alerts                                          | Show "My Notifications" page                                                                                     |
| appvp://orders                                          | Show "My orders" page                                                                                            |
| appvp://cart                                            | Show cart page                                                                                                   |
| appvp://lastcart/{cartId}                               | Show last abandonned cart dialog for the Order *orderId*                                                         |
| appvp://browser/{url}                                   | Open the browser (chrome, ...) with the given *url* (url encoded)                                                |
| appvp://openwv/{url}                                    | Open a webview with the given *url* (url encoded)                                                                |
| appvp://search/{searchTerm}                             | Open the search result page with the *searchTerm*                                                                |
| appvp://compte                                          | Show "My account" page                                                                                           |
| appvp://memberdata                                      | Show "My personal details" page                                                                                  |
| appvp://communicationvp                                 | Show "The latest from vente-privee" page (inside My Notifications)                                               |
| appvp://faq                                             | Show FAQ page                                                                                                    |
| appvp://contact                                         | Show "Contact us" page                                                                                           |
| appvp://help                                            | Show "Need help ?" page                                                                                          |
| appvp://legal                                           | Show "Legal notes" page                                                                                          |
| appvp://privacy                                         | Show "Privacy Policy" page                                                                                       |
| appvp://mediation                                       | Show Mediation" page                                                                                             |
| appvp://operation/{operationId}                         | Open operation home page with the specific *#operationId*                                                        |
| appvp://operation/{operationId}/all                     | Open the operation catalog with all available products                                                           |
| appvp://fp/{operationId}/{universId}                    | Open Product detail page                                                                                         |
| appvp://fp/{operationId}/{universId}/{productsFamilyId} | Open Product detail page                                                                                         |
| appvp://fp/{operationId}//{productsFamilyId} | Open Special Product detail page (empty _universId_)                                                                        |
| appvp://specialfp/{operationId}/{productsFamilyId}      | Open Special Product detail page                                                                                 |
| appvp://cgu                                             | Show CGU                                                                                                         |
| appvp://cgv                                             | Show CGV                                                                                                         |
| appvp://refunds                                         | Show "My gift vouchers" page                                                                                     |
| appvp://sponsor                                         | Show "My godchildren" page                                                                                       |
| appvp://address                                         | Show "My addresses" page                                                                                         |
| appvp://communicationbrands                             | Show "My brand alerts" page (inside My Notifications)                                                            |
| appvp://paycards                                        | Show "My cards" page                                                                                             |
| appvp://subscriptions                                   | Show "My subscriptions" page                                                                                     |
| appvp://opendialog/{icefox}                             | Open a dialog by displaying the content of an icefox key. The ".title" & ".text" are added to the end by the app.|
| appvp://specialcatalog/{operationId}/{filterId1}/{filterId2}  | Open the special catalog page with *operationId* filter by filters *filterId1* *filterId2* until *filterIdX* |
| appvp://sousnav/{homeId}/{categoryId}                   | Open the operations home page  at the specific home *#homeId* filtered with the *#categoryId*                    |
| appvp://creation                                        | Show registration form                                                                                           |
| appvp://creationoperation/{operationCode}/{operationId}/{siteId} | Show the registration form with an operation banner at the top                                          |
| appvp://login                                           | Show login page                                                                                                  |
| appvp://loginvex                                        | Used by VenteExclusive to retrieve VP member login informations (not implemented in iOS)                         |
| appvp://subscribeconfirm                                | Validate registered account (double optin DE)                                                                    |
| appvp://specialhome/{categoryId}                        | Open a special event home                                                                                        |
| appvp://securedwebview/{url}                            | Open a Webview with the url after being secured with "operation/getsecuredexternalurl" WS						             |
| appvp://notificationsettings                            | Open the notification settings of the app in the system															                             |

Source : https://git.vptech.eu/veepee/offerdiscovery/products/front-mobile/common/mobile-deeplinks/-/blob/master/README.md
